# ZFS Monitor 

This simple program is used to monitor the status of your ZFS pools and send email statuses using Sendgrid. It is heavily inspired by [this Bash script](https://calomel.org/zfs_health_check_script.html). Like the original script it checks for:

- Health - Check if all zfs volumes are not degraded or broken in any way.
- Capacity - Make sure all pool capacities are below 80% for best performance.
- Errors - Check the columns for READ, WRITE and CKSUM (checksum) drive errors.
- Scrub Expired - Check if all volumes have been scrubbed in at least the last 8 days (configurable).

## Installation

I currently compile binaries for `x86_64`. You can find them in the [releases section](https://gitlab.com/binaryronin/go-zfs-monitor/-/releases). If you need another architecture, or want to compile it yourself follow the standard Go process:
```
$ go mod download
$ go build -o zfs-monitor
```

## Usage

I recommend using this with an `.env` file for the necessary variables. You will need to set the env with the follow:
```
SCRUB_DAYS=30
FROM_EMAIL=from@youremail.com
TO_EMAIL=you@youremail.com
SENDGRID_API_KEY=SG.superlongkeygoeshereaaahhhhhhhhhhhhh
```

Then simply call the program with the `-env` switch: `zfs-monitor -env location.of.env`

You can also call the program by putting all the necessary variables on the command line:
`zfs-monitor -from from@youremail.com -to you@youremail.com -sgapi SG.superlongkeygoeshere`

By default you will get a status email whether there are errors or not on the pool. If you only want emails on errors, then use the `-error-only` switch.

Finally, you can also override the default scrub days with `-scrub` and the number of days. If you set nothing, the default is 8 days. Please note that using an env will override anything on the command line.

## Reasons
### Why Golang?

Because I'm looking for more excuses to use the language. Also, honestly, my Bash scripting is subpar and when the script above broke for me, it was going to be more of a hassle to fix it then to just write it in something I knew better. I originally wrote this in Python, but I like the complied nature of Go and being able to have a single binary with no other dependencies. It's honestly perfect for a server.

### Why Sendgrid?

Because using `sendmail` broke for some reason on my server that I have yet to discover. And because they let you send 100 emails a day for free; which is far more than I am ever going to send.