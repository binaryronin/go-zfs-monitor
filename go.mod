module zfs-monitor

go 1.16

require (
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/sendgrid/rest v2.6.7+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.10.5+incompatible // indirect
)
